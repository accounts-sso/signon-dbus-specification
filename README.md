Signon DBus Specification
=========================

This repository contains the common Signon specification used across all projects

> This project is made to allow anyone to chose the wanted daemon implementation.
> Any library should work with any daemon.

Content
-------

* [com.google.code.AccountsSSO.SingleSignOn.AuthService.xml](com.google.code.AccountsSSO.SingleSignOn.AuthService.xml)
* [com.google.code.AccountsSSO.SingleSignOn.AuthSession.xml](com.google.code.AccountsSSO.SingleSignOn.AuthSession.xml)
* [com.google.code.AccountsSSO.SingleSignOn.Identity.xml](com.google.code.AccountsSSO.SingleSignOn.Identity.xml)
